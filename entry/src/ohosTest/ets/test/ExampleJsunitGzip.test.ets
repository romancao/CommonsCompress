/*
* Copyright (C) 2021 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { gzipFile, unGzipFile } from '@ohos/commons-compress';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { GlobalContext } from '../testability/gloalThis';

export default function exampleJsunitGzip() {
  describe('gzipCompressTest', (done:Function)=> {


    /*
     * @tc.number: gzip_compress_test_001
     * @tc.name: gzipCompress
     * @tc.desc: gzip compress Success DeCompress Success
     */
    it('gzip_compress_test_001', 0, (done:Function)=> {
      console.log("==================gzip_compress_test_001 start==================");
      try {
        // globalThis.fileDir=this.context.filesDir
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath = data + "/new"
        let src001 = srcPath + "/hello.txt";
        let gzipDest001 = data + "/newFolder.txt.gz";
        let ungzipDir001 = data + "/newFolder.txt.gz";
        let ungzipNew001 = data + "/newTarget";
        let ungzipDest001 = ungzipNew001 + "/newFolder.txt";
        try {
          fileio.mkdirSync(srcPath);
        } catch (err) {
        }
        const writer = fileio.openSync(src001, 0o102, 0o666)
        fileio.writeSync(writer, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fileio.closeSync(writer);
        gzipFile(src001, gzipDest001).then((isSuccess) => {
          let gzipStat = fileio.statSync(gzipDest001);
          let isFile = gzipStat.isFile();
          expect(isFile).assertTrue();
          let srcSize = fileio.statSync(src001).size;
          console.log('gzip_compress_test_001 1:' + srcSize);
          let gzipDestSize = gzipStat.size;
          console.log('gzip_compress_test_001 2:' + gzipDestSize);
          expect(srcSize >= gzipDestSize).assertTrue();
          expect(isSuccess).assertTrue();

          try {
            fileio.mkdirSync(ungzipNew001);
          } catch (err) {
          }

          unGzipFile(ungzipDir001, ungzipDest001)
            .then((ungzipIsSuccess) => {
              let ungzipStat = fileio.statSync(ungzipDest001);
              let isFile = ungzipStat.isFile();
              expect(isFile).assertTrue();
              console.log('gzip_compress_test_001 3:' + isFile);
              let ungzipDestSize = ungzipStat.size;
              console.log('gzip_compress_test_001 4:' + ungzipDestSize);
              let originSize = fileio.statSync(src001).size;
              console.log('gzip_compress_test_001 5:' + originSize);
              let ungzipResult = (originSize == ungzipDestSize);
              expect(ungzipResult).assertTrue();
              expect(ungzipIsSuccess).assertTrue();
              done();
              console.log("==================gzip_compress_test_001 end==================");
            })
        });
      } catch (err) {
        console.error('gzip_compress_test_001 err:' + err);
        done();
        console.log("==================gzip_compress_test_001 end==================");
      }
    })

    /*
     * @tc.number: gzip_compress_test_002
     * @tc.name: gzipCompress
     * @tc.desc: gzip compress Success DeCompress Success
     */
    it('gzip_compress_test_002', 0, (done:Function)=> {
      console.log("==================gzip_compress_test_002 start==================");
      try {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath:string = data + "/new"
        let src001:string = srcPath + "/hello1111.txt";
        let gzipDest001:string = data + "/hello1111.txt.gz";
        let ungzipDir001:string = data + "/hello1111.txt.gz";
        let ungzipNew001:string = data + "/gzipNewPath";
        let ungzipDest001:string = ungzipNew001 + "/hello1111.txt";
        try {
          fileio.mkdirSync(srcPath);
        } catch (err) {
        }
        const writer = fileio.openSync(src001, 0o102, 0o666)
        fileio.writeSync(writer, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fileio.closeSync(writer);
        gzipFile(src001, gzipDest001).then((isSuccess) => {
          let gzipStat = fileio.statSync(gzipDest001);
          let isFile = gzipStat.isFile();
          expect(isFile).assertTrue();
          let srcSize = fileio.statSync(src001).size;
          console.log('gzip_compress_test_002 1:' + srcSize);
          let gzipDestSize = gzipStat.size;
          console.log('gzip_compress_test_002 2:' + gzipDestSize);
          expect(srcSize >= gzipDestSize).assertTrue();
          expect(isSuccess).assertTrue();

          try {
            fileio.mkdirSync(ungzipNew001);
          } catch (err) {
          }

          unGzipFile(ungzipDir001, ungzipDest001)
            .then((ungzipIsSuccess) => {
              let ungzipStat = fileio.statSync(ungzipDest001);
              let isFile = ungzipStat.isFile();
              expect(isFile).assertTrue();
              console.log('gzip_compress_test_002 3:' + isFile);
              let ungzipDestSize = ungzipStat.size;
              console.log('gzip_compress_test_002 4:' + ungzipDestSize);
              let originSize = fileio.statSync(src001).size;
              console.log('gzip_compress_test_002 5:' + originSize);
              let ungzipResult = (originSize == ungzipDestSize);
              expect(ungzipResult).assertTrue();
              expect(ungzipIsSuccess).assertTrue();
              done();
              console.log("==================gzip_compress_test_002 end==================");
            })
        });
      } catch (err) {
        console.error('gzip_compress_test_002 err:' + err);
        done();
        console.log("==================gzip_compress_test_002 end==================");
      }
    })

  })
}
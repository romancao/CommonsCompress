/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import {
  subParameters,
  LZ77Compressor,
  Block,
  LiteralBlock,
  AbstractLZ77CompressorInputStream,
  Arrays,
  InputStream,
  ByteArrayInputStream,
  ByteUtils,
  Long
} from '@ohos/commons-compress';

export default function LZ77CompressorTest() {

  describe('LZ77CompressorTest', ()=> {

    it('LZ77CompressorTest_001', 0, (done:Function)=> {
      let ONE_TO_TEN: Int8Array = new Int8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
      let blocks: Array<Block> = compress(newParameters1(128), ONE_TO_TEN);
      expect(blocks.length == 2).assertTrue();
      done()
    })

    it('LZ77CompressorTest_002', 0, (done:Function)=> {
      let ONE_TO_TEN: Int8Array = new Int8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
      let blocks: Array<Block> = compress(newParameters1(8), ONE_TO_TEN);
      expect(blocks.length == 3).assertTrue();
      done()
    })

    it('LZ77CompressorTest_003', 0, (done:Function)=> {
      let str1: string = "Blah blah blah blah blah!";
      let BLA: Int8Array = getBytes(str1);
      let blocks: Array<Block> = compress(newParameters1(128), BLA);
      expect(blocks.length == 4).assertTrue();
      done()
    })

    it('LZ77CompressorTest_004', 0, (done:Function)=> {
      let str1: string = "Blah blah blah blah blah!";
      let BLA: Int8Array = getBytes(str1);
      let blocks: Array<Block> = compress(newParameters(128, 3, 5, 0, 0), BLA);
      expect(blocks.length == 7).assertTrue();
      done()
    })

    it('LZ77CompressorTest_005', 0, (done:Function)=> {
      let str2: string = "I am Sam\n"
      + "\n"
      + "Sam I am\n"
      + "\n"
      + "That Sam-I-am!\n"
      + "That Sam-I-am!\n"
      + "I do not like\n"
      + "that Sam-I-am!\n"
      + "\n"
      + "Do you like green eggs and ham?\n"
      + "\n"
      + "I do not like them, Sam-I-am.\n"
      + "I do not like green eggs and ham.";

      let SAM: Int8Array = getBytes(str2)
      let blocks: Array<Block> = compress(newParameters1(1024), SAM);
      expect(blocks.length == 21).assertTrue();
      done()
    })

    it('LZ77CompressorInputStreamTest', 0, (done:Function)=> {
      let data: Int8Array = new Int8Array([1, 2, 3, 4]);
      let s: TestStream = new TestStream(new ByteArrayInputStream(ByteUtils.EMPTY_BYTE_ARRAY, 0, 0))
      try {
        s.prefill(data);
        s.startBackReference(2, Long.fromNumber(4));
        let r: Int8Array = new Int8Array(4);
        expect(s.read() == 3).assertTrue()
        expect(s.readBytes(r) == 4).assertTrue()
        done()
      } catch {
      }
    })

    it('ParametersTest_001', 0, (done:Function)=> {
      let p: subParameters = newParameters1(128);
      expect(p.getWindowSize() == 128).assertTrue();
      expect(p.getMinBackReferenceLength() == 3).assertTrue();
      expect(p.getMaxBackReferenceLength() == 127).assertTrue();
      expect(p.getMaxOffset() == 127).assertTrue();
      expect(p.getMaxLiteralLength() == 128).assertTrue();
      done()
    })

    it('ParametersTest_002', 0, (done:Function)=> {
      let p: subParameters = newParameters(128, 2, 3, 4, 5);
      expect(p.getMaxBackReferenceLength() == 3).assertTrue();
      done()
    })
  })
}

function getBytes(str: string) {
  let arr:number[] = []
  for (let i = 0; i < str.length; i++) {
    let char = str.charCodeAt(i)
    arr.push(char as never)
  }
  return new Int8Array(arr)
}

function getStr(arr: Int8Array, str: string) {
  arr.forEach((item) => {
    str = str.concat(String.fromCharCode(item))
  })
}

function compress(params: subParameters, chunks: Int8Array): Array<Block> {
  let blocks: Array<Block> = new Array<Block>();
  let c: LZ77Compressor = new LZ77Compressor(params, (block) => {
    //System.err.println(block);
    if (block instanceof LiteralBlock) {
      // replace with a real copy of data so tests
      // can see the results as they've been when
      // the callback has been called
      let b: LiteralBlock = block as LiteralBlock;
      let len: number = b.getLength();
      block = new LiteralBlock(
        Arrays.copyOfRangeInt(new Int32Array(b.getData()), b.getOffset(), b.getOffset() + len),
        0, len);
    }
    blocks.push(block);
  });

  c.compressByte(chunks);
  c.finish();
  return blocks;
}

function newParameters1(windowSize: number): subParameters {
  return subParameters.builder(windowSize).build();
}

function newParameters(windowSize: number, minBackReferenceLength: number, maxBackReferenceLength: number,
                       maxOffset: number, maxLiteralLength: number): subParameters {
  return subParameters.builder(windowSize)
    .withMinBackReferenceLength(minBackReferenceLength)
    .withMaxBackReferenceLength(maxBackReferenceLength)
    .withMaxOffset(maxOffset)
    .withMaxLiteralLength(maxLiteralLength)
    .build();
}

class TestStream extends AbstractLZ77CompressorInputStream {
  private literal: boolean | false = false;

  constructor(input: InputStream) {
    super(input, 1024);
  }

  public readBytesOffset(b: Int8Array, off: number, len: number): number {
    if (this.literal) {
      return this.readLiteral(b, off, len);
    }
    return this.readBackReference(b, off, len);
  }

  literal1(len: number): void {
    this.startLiteral(Long.fromNumber(len));
    this.literal = true;
  }
}

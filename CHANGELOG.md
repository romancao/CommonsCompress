## 2.0.1
1.ArkTs语法规则整改
## 2.0.0
1.包管理工具由npm切换为ohpm

2.适配DevEco Studio版本: 4.0Canary1(4.0.3.212)

3.适配SDK: API10(4.0.8.3)

4.Exception依赖库ts-custom-error改用系统的 Error

5.添加 snappy和lz4格式解压缩功能接口 返回boolean

## 1.0.6
1.zip压缩解压能力改为 @ohos.zlib 系统接口实现。

## 1.0.5
1.适配DevEco Studio 3.1 Beta1版本。

## 1.0.4
1.优化zstd压缩解压性能。

## 1.0.3
1.支持Stage模型。

## 1.0.2
1.由gradle工程整改为hvigor工程。

## 1.0.1
1.适配ark引擎。

## 1.0.0
1.支持bzip2、gzip、lzma、xz、Snappy、LZ4、cpio、deflate、Zstandard 和 ar、tar、zip、7z等格式的压缩/解压功能。

2.支持Brotli、dump、z等格式解压功能。